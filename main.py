#!/usr/bin/env python3

# train a multi-layer perceptron to compute XOR

import os

import matplotlib.pyplot as plt
#import numpy as np
import tensorflow as tf

def train (a, b, c, xs, ys, rate):
  with tf.GradientTape() as tape:
    y = network (a, b, c, xs)
    loss = tf.nn.l2_loss (y - ys)
  da, db, dc, dxs = tape.gradient (loss, [a, b, c, xs])
  a.assign_sub (rate * da)
  b.assign_sub (rate * db)
  c.assign_sub (rate * dc)
  return loss

# 2-layer perceptron network
"""
def network (a, b, c, x):
  y1 = [[threshold (0.0, dense (a, x))], [threshold (0.0, dense (b, x))]]
  return threshold (0.0, dense (c, y1))
"""
def network (a, b, c, x):
  y1 = [tf.math.sigmoid(dense (a, x))[0], tf.math.sigmoid (dense (b, x))[0]]
  return tf.math.sigmoid(dense (c, y1))

# dense layer
def dense (w, x):
  return tf.linalg.matmul (w[:, 1:], x) + w[:, 0]

print ("main...")

# suppress info messages from .cc source files
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "1"

print ("tensorflow version: ", tf.version.VERSION)
print ("eager execution:    ", tf.executing_eagerly())
print ("physical devices:   ", tf.config.list_physical_devices())
print ("logical devices:    ", tf.config.list_logical_devices())
#np.set_printoptions (linewidth=200)

a = tf.Variable (tf.random.normal (shape=[1, 3]), dtype="float32")
b = tf.Variable (tf.random.normal (shape=[1, 3]), dtype="float32")
c = tf.Variable (tf.random.normal (shape=[1, 3]), dtype="float32")

print ("a:", a.numpy())
print ("b:", b.numpy())
print ("c:", c.numpy())

dataset = (
  tf.Variable ([
    [0.0, 0.0],
    [0.0, 1.0],
    [1.0, 0.0],
    [1.0, 1.0]
  ], dtype="float32"),
  tf.Variable ([0.0, 1.0, 1.0, 0.0], dtype="float32")
)
print ("dataset:", dataset)

print ("network (0, 0):", network (a, b, c, [[0.0], [0.0]]).numpy())
print ("network (1, 0):", network (a, b, c, [[1.0], [0.0]]).numpy())
print ("network (0, 1):", network (a, b, c, [[0.0], [1.0]]).numpy())
print ("network (1, 1):", network (a, b, c, [[1.0], [1.0]]).numpy())

mygraphfun = tf.function (network)
print ("mygraphfun (0, 0):", mygraphfun (a, b, c, [[0.0], [0.0]]).numpy())
print ("mygraphfun (1, 0):", mygraphfun (a, b, c, [[1.0], [0.0]]).numpy())
print ("mygraphfun (0, 1):", mygraphfun (a, b, c, [[0.0], [1.0]]).numpy())
print ("mygraphfun (1, 1):", mygraphfun (a, b, c, [[1.0], [1.0]]).numpy())
mygraphtrain = tf.function (train)

epochs = range (1000)
rate = 1.0
losses = []

for epoch in epochs:
  loss = 0.0
  loss += mygraphtrain (a, b, c, tf.Variable (dataset[0][0].numpy().reshape (2, 1)),
    dataset[1][0], rate)
  loss += mygraphtrain (a, b, c, tf.Variable (dataset[0][1].numpy().reshape (2, 1)),
    dataset[1][1], rate)
  loss += mygraphtrain (a, b, c, tf.Variable (dataset[0][2].numpy().reshape (2, 1)),
    dataset[1][2], rate)
  loss += mygraphtrain (a, b, c, tf.Variable (dataset[0][3].numpy().reshape (2, 1)),
    dataset[1][3], rate)
  losses.append (loss)

print ("network (0, 0):", network (a, b, c, [[0.0], [0.0]]).numpy())
print ("network (1, 0):", network (a, b, c, [[1.0], [0.0]]).numpy())
print ("network (0, 1):", network (a, b, c, [[0.0], [1.0]]).numpy())
print ("network (1, 1):", network (a, b, c, [[1.0], [1.0]]).numpy())

plt.plot (losses)
plt.savefig ("losses.png")

os.system ("feh losses.png")

print ("...main")
