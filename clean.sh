#!/usr/bin/env bash

set -x

rm -rf logs __pycache__ _build losses.png

exit 0
