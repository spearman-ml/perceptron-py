with import <nixpkgs> {};
mkShell {
  buildInputs = [
    (python39.withPackages (p: with p; [
      tensorflow
      tensorflow-tensorboard
    ]))
  ];
}
