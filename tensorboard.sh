#!/usr/bin/env bash

set -x
firefox http://localhost:6006 &> /dev/null | tensorboard --logdir ./logs

exit 0
